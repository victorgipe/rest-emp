package com.techuniversity.emp.model;

import java.util.ArrayList;
import java.util.List;

public class Empleados {

    private ArrayList<Empleado> empleados;

    public List<Empleado> getEmpleados() {
        if (this.empleados == null){
            this.empleados = new ArrayList<Empleado>();
        }
        return empleados;
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }
}
