package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
class RestEmpApplicationTests {

	@Autowired
	EmpleadosController empleadosController;

	@Test
	void contextLoads() {
	}

	@Test
	public void testHome(){
		String result = empleadosController.getHome();
		assertEquals(result, "home");
	}

	@Test
	public void testBadSeparator(){
		try{
			Utilidades.getCadena("Victor Gil", "..");
			fail("Se esperaba Bad Separator");
		} catch (BadSeparator bs){
			bs.getMessage();
		}
	}

}
